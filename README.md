# @krlwlfrt/async-pool

[![pipeline status](https://img.shields.io/gitlab/pipeline/krlwlfrt/async-pool.svg?style=flat-square)](https://gitlab.com/krlwlfrt/async-pool/commits/master) 
[![npm](https://img.shields.io/npm/v/@krlwlfrt/async-pool.svg?style=flat-square)](https://npmjs.com/package/@krlwlfrt/async-pool)
[![license)](https://img.shields.io/npm/l/@krlwlfrt/async-pool.svg?style=flat-square)](https://opensource.org/licenses/MIT)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://krlwlfrt.gitlab.io/async-pool)

This is a TypeScript enabled version of [tiny-async-pool](https://github.com/rxaviers/async-pool).

## Usage

```typescript
import {asyncPool} from '@krlwlfrt/async-pool/lib/async-pool';

// can be anything that is iterable (aka implements Iterable)
const inputIterable = [100, 200, 300, 400];

/**
* Multiply input number by two
* 
* @param item Number to multiply by two
*/
async function iteratorFunction (item: number): Promise<number> {
  return item * 2;
}

// invoke async pool with limit 2 - meaning 2 iterator functions are executed simultaneously
asyncPool(2, inputIterable, iteratorFunction).then((result) => {
  console.log(result); // outputs [ 200, 400, 600, 800 ]
});
```

## Documentation

See [online documentation](https://krlwlfrt.gitlab.io/async-pool) for detailed API description.
