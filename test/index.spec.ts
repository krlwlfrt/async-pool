import {slow, suite, test, timeout} from '@testdeck/mocha';
import {asyncPool} from '../src/index';
import {deepStrictEqual, rejects} from 'assert';


/**
 * Sleep for a number of milliseconds
 *
 * @param ms Number of milliseconds to wait
 */
async function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/**
 * A dummy iterable
 */
class DummyIterable implements IterableIterator<number> {
  private idx = 0;

  constructor(private iterableItems: number[]) {
  }

  [Symbol.iterator](): IterableIterator<number> {
    return this;
  }

  next(): IteratorResult<number> {
    if (this.idx < this.iterableItems.length) {
      return {
        done: false,
        value: this.iterableItems[this.idx++],
      };
    }

    return {
      done: true,
      value: NaN,
    };
  }
}

const items = [800, 700, 900, 600];

@suite('AsyncPoolSpec', timeout(10000), slow(50000))
export class AsyncPoolSpec {
  @test
  public async shouldReject() {
    return rejects(async () => {
      await asyncPool(2, items, async (i: number): Promise<number> => {
        await sleep(i);

        if (i === 600) {
          throw new Error(i.toString());
        }

        return i * 2;
      });
    });
  }

  @test
  public async shouldWork() {
    const results = await asyncPool(2, items, async (i: number): Promise<number> => {
      await sleep(i);
      return i * 2;
    });

    deepStrictEqual(results, items.map((x) => x * 2));
  }

  @test
  public async shouldWorkOnEmptyInput() {
    const results = await asyncPool(2, [], async (i: number): Promise<number> => {
      await sleep(i);
      return i * 2;
    });

    deepStrictEqual(results, []);
  }

  @test
  public async shouldWorkWithIterable() {
    const results = await asyncPool(2, new DummyIterable(items), async (i: number): Promise<number> => {
      await sleep(i);
      return i * 2;
    });

    deepStrictEqual(results, items.map((x) => x * 2));
  }

  @test
  public async shouldWorkWithString() {
    const results = await asyncPool(2, 'Foobar', async (i: string): Promise<string> => {
      if (i.match(/^[A-Z]$/)) {
        return i.toLowerCase();
      }

      return i.toUpperCase();
    });

    deepStrictEqual(results, ['f', 'O', 'O', 'B', 'A', 'R']);
  }
}
