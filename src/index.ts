/**
 * Runs multiple promise-returning & async functions in a limited concurrency pool
 * - It rejects immediately as soon as one of the promises rejects.
 * - It resolves when all the promises complete.
 * - It calls the iterator function as soon as possible (under concurrency limit).
 * @param poolLimit The pool limit number (>= 1)
 * @param items Iterable list of items
 * @param iteratorFunction Iterator function that takes one item of the list as argument.
 *                         The iterator function should either return a promise or be an async function.
 * @template IN Type of the input items
 * @template OUT Type of the resolves of the promises
 * @returns Array of promises that resolve with results of inputs
 */
export async function asyncPool<IN, OUT>(
  poolLimit: number,
  items: Iterable<IN>,
  iteratorFunction: (generator: IN) => Promise<OUT>,
): Promise<OUT[]> {
  // instantiate promises array
  const promises: Promise<OUT>[] = [];

  // instantiate pool array
  const pool: Promise<void>[] = [];

  // iterate over all items
  for (const item of items) {
    // run iterator function on item
    const promise = iteratorFunction(item);

    // add to promises array
    promises.push(promise);

    // instantiate pool member
    const poolMember: Promise<void> = promise.then(() => {
      // remove pool member from pool if it resolves
      // eslint-disable-next-line no-magic-numbers
      pool.splice(pool.indexOf(poolMember), 1);
    });

    // add pool member to pool array
    pool.push(poolMember);

    // check if pool is full
    if (pool.length >= poolLimit) {
      // wait for first promise to resolve
      await Promise.race(pool);
    }
  }

  // wait for all promises to resolve and return their results
  return Promise.all(promises);
}
