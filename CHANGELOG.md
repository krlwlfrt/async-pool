# [1.1.0](https://gitlab.com/krlwlfrt/async-pool/compare/v1.0.0...v1.1.0) (2024-11-25)



# [1.0.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.8.0...v1.0.0) (2023-02-28)



# [0.8.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.7.0...v0.8.0) (2022-09-12)



# [0.7.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.6.0...v0.7.0) (2021-07-26)



# [0.6.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.5.0...v0.6.0) (2021-04-06)



# [0.5.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.4.1...v0.5.0) (2021-02-22)



## [0.4.1](https://gitlab.com/krlwlfrt/async-pool/compare/v0.4.0...v0.4.1) (2020-05-11)



# [0.4.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.3.0...v0.4.0) (2020-03-09)


### Bug Fixes

* update dependencies ([d58628f](https://gitlab.com/krlwlfrt/async-pool/commit/d58628f887db775facc91848188b8606f9558b96)), closes [#2](https://gitlab.com/krlwlfrt/async-pool/issues/2)



# [0.3.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.2.1...v0.3.0) (2020-01-08)



## [0.2.1](https://gitlab.com/krlwlfrt/async-pool/compare/v0.2.0...v0.2.1) (2019-11-25)


### Bug Fixes

* correct paths to built files ([4100eea](https://gitlab.com/krlwlfrt/async-pool/commit/4100eeadb7cd43e625eecf575ca152e671ea8e3b))



# [0.2.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.1.0...v0.2.0) (2019-11-25)



# [0.1.0](https://gitlab.com/krlwlfrt/async-pool/compare/v0.0.3...v0.1.0) (2019-04-16)



## [0.0.3](https://gitlab.com/krlwlfrt/async-pool/compare/v0.0.2...v0.0.3) (2019-04-08)



## [0.0.2](https://gitlab.com/krlwlfrt/async-pool/compare/v0.0.1...v0.0.2) (2019-04-02)



## [0.0.1](https://gitlab.com/krlwlfrt/async-pool/compare/562082b0dbfa5b1c0d983d717e7f5ebcf3f53769...v0.0.1) (2019-04-02)


### Features

* add implementation ([562082b](https://gitlab.com/krlwlfrt/async-pool/commit/562082b0dbfa5b1c0d983d717e7f5ebcf3f53769))



